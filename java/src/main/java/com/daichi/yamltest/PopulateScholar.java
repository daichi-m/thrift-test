package com.daichi.yamltest;
import com.daichi.yamltest.thrift.Scholar;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by daichi on 9/12/15.
 */
public class PopulateScholar {

    private List<Scholar> getScholarList() {
        return new ArrayList<Scholar>();
    }

    public <T> T getScholar(File yamlFile, Class<T> type) {

        if(type.equals(Scholar.class)) {
            return (T)new Scholar();
        } else if (type.equals(List.class)) {
            return (T)new ArrayList<Scholar>();
        } else {
            throw new UnsupportedOperationException();
        }
    }
}
