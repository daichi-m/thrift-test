namespace java com.daichi.yamltest.thrift

struct Scholar {
    1: required string name,
    2: required double aggregate,
    3: required bool scholarshipEligible,
    4: optional bool scholarshipApplied
}